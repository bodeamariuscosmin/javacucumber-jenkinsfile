@test
Feature: BMC homepage tests

  Scenario: BMC Homepage Title Test
    Given I navigate to "homepage"
    Then the title of the page should be "BMC - Homepage"
    When I click the "Log In" button from header