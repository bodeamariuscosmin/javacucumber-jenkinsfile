package cucumber.pages;

import framework.BaseClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class NavigationHeaderPage extends BaseClass {

    @FindBy(linkText = "Log In")
    public WebElement logInButton;

    public NavigationHeaderPage() { }

    public NavigationHeaderPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
}
